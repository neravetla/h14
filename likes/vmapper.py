#opens file in read format
i = open("data.csv", "r")
#opens file in write format
o = open("m.csv","w")

#line counter
countLine = 0

for line in i:
    data = line.strip().split(",") #this remove any white space, and turns the rest into a list
    countLine += 1
    if len(data) == 4:#checks if it is the right input
        id, text, likes, replies = data
        if countLine >= 2:#ignores the header
            o.write("{0},{1}\n".format(id,1))#write to file
i.close()#closed file IO
o.close()
