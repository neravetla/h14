#Solution: Highest count is 725


#opens sorted.csv in read mode
s = open("s.csv", "r")
#opens REDUCED.txt in write mode
r = open("r.csv","w")
lines = s.readlines()

currentId = "" #keeps track of current video id
currentIdCounter = 0 #keeps track of number of comments on current video id
maxId = None #keeps track of video_id with most comments
maxCounter = 0 #keeps track of number of comments on video with most comments

r.write("id,count\n")

for line in lines:
    data = line.strip().split(",")
    if len(data) == 2:
        id, comment = data
        if currentId != id:#if key changes, flush the data to disk, then change the variables
            if currentId:
                r.write(currentId+','+str(currentIdCounter)+'\n')
            if maxCounter < currentIdCounter:#check if the maxcounter is less, if it is, it updates.
                maxCounter = currentIdCounter
                maxId = currentId
            currentIdCounter = 1
            currentId = id
            continue
        currentIdCounter = currentIdCounter + 1
r.write(currentId+','+str(currentIdCounter)+'\n')
if maxCounter < currentIdCounter:#final maxcount check
    maxCounter = currentIdCounter
    maxId = currentId
print("video id with maximum number of comments:")
print("{0}, {1}".format(maxId, maxCounter))
r.close()
s.close()
